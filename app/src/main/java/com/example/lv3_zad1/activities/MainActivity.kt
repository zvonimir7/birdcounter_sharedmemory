package com.example.lv3_zad1.activities

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.lv3_zad1.LV3_Zad1
import com.example.lv3_zad1.R
import com.example.lv3_zad1.databinding.ActivityMainBinding
import com.example.lv3_zad1.utilities.getColorResource
import java.lang.Error
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var sharedPreferencesEditor: SharedPreferences.Editor
    private val settingsPathFileName = "Settings"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        mainBinding.btnColor1.setOnClickListener { color1Update() }
        mainBinding.btnColor2.setOnClickListener { color2Update() }
        mainBinding.btnColor3.setOnClickListener { color3Update() }
        mainBinding.btnColor4.setOnClickListener { color4Update() }
        mainBinding.btnResetCounter.setOnClickListener { resetCounter() }

        initializeBG()
        initializeUI()
        setContentView(mainBinding.root)
    }

    private fun initializeUI() {
        val counter = sharedPreferences.getInt("birdsCounter", 0)
        val lastBirdSeenColor = sharedPreferences.getString("lastBirdSeen", "")

        if (lastBirdSeenColor != null) {
            updateCounter(counter, lastBirdSeenColor)
        }
    }

    private fun initializeBG() {
        sharedPreferences = getSharedPreferences(settingsPathFileName, Context.MODE_PRIVATE)
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    private fun updateCounter(counter: Int, color: String) {
        mainBinding.txtCounter.text = "Counter: $counter"
        mainBinding.txtCounter.setBackgroundResource(getColorResource(color))
    }

    private fun incrementSharedStorage(color: String) {
        sharedPreferencesEditor.putString("lastBirdSeen", color);
        val counter = sharedPreferences.getInt("birdsCounter", 0)
        sharedPreferencesEditor.putInt("birdsCounter", counter + 1).apply()

        updateCounter(counter + 1, color)
    }

    private fun color1Update() = incrementSharedStorage(mainBinding.btnColor1.text.toString())

    private fun color2Update() = incrementSharedStorage(mainBinding.btnColor2.text.toString())

    private fun color3Update() = incrementSharedStorage(mainBinding.btnColor3.text.toString())

    private fun color4Update() = incrementSharedStorage(mainBinding.btnColor4.text.toString())

    private fun resetCounter() {
        sharedPreferencesEditor.putString("lastBirdSeen", "");
        sharedPreferencesEditor.putInt("birdsCounter", 0).apply()

        updateCounter(0, "")
    }
}