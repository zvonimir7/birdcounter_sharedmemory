package com.example.lv3_zad1.utilities
import com.example.lv3_zad1.R

fun getColorResource(color: String): Int{
    return when(color){
        "Red" -> R.color.red
        "Green" -> R.color.green
        "Blue" -> R.color.blue
        "Yellow" -> R.color.yellow
        else -> R.color.white
    }
}