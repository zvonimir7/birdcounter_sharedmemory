package com.example.lv3_zad1

import android.app.Application

class LV3_Zad1 : Application() {

    companion object{
        lateinit var application: LV3_Zad1
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }
}